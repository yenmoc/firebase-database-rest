﻿/*

Class: DataSnapshot.cs
==============================================
Last update: 2018-05-20  (by Dikra)
==============================================


 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System.Collections.Generic;
using System.Security;
using UnityModule.RestFirebase.MiniJSON;

// ReSharper disable once CheckNamespace
namespace UnityModule.RestFirebase
{
    public class DataSnapshot
    {
        protected object valObj;
        protected Dictionary<string, object> valDict;
        protected List<string> keys;
        protected string json;

        protected DataSnapshot()
        {
            valDict = null;
            valObj = null;
            keys = null;
            json = null;
        }

        /// <summary>
        /// Creates snapshot from Json string 
        /// </summary>
        /// <param name="json">Json string</param>
        public DataSnapshot(string json = "")
        {
            object obj = !string.IsNullOrEmpty(json) ? Json.Deserialize(json) : null;

            if (obj is Dictionary<string, object>)
                valDict = obj as Dictionary<string, object>;
            else
                valObj = obj;

            keys = null;
            this.json = json ?? "";
        }

        /// <summary>
        /// If snapshot is a Dictionary, returns keys of the snapshot , else null
        /// </summary>
        public List<string> Keys
        {
            get
            {
                if (keys == null && valDict != null)
                    keys = new List<string>(valDict.Keys);

                return keys;
            }
        }

        /// <summary>
        /// If snapshot is a Dictionary, gives the first key founded on snapshot, else null
        /// </summary>
        public string FirstKey => (valDict == null) ? null : Keys[0];

        /// <summary>
        /// Raw json of snapshot
        /// </summary>
        public string RawJson => json;

        /// <summary>
        /// Raw value of snapshot
        /// </summary>
        public object RawValue => valDict ?? valObj;

        /// <summary>
        /// Gets value from snapshot
        /// </summary>
        /// <typeparam name="T">Desired type</typeparam>
        /// <returns>Value of snapshot as the defined type, null if typecasting failed</returns>
        [SecuritySafeCritical]
        public T Value<T>()
        {
            try
            {
                if (valObj != null)
                    return (T) valObj;
                object obj = valDict;
                return (T) obj;
            }
            catch
            {
                return default;
            }
        }
    }
}