﻿/*

Class: FirebaseQueue.cs
==============================================
Last update: 2018-05-20  (by Dikra)
==============================================


 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityModule.RestFirebase.MiniJSON;

// ReSharper disable DelegateSubtraction
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.RestFirebase
{
    public class FirebaseQueue
    {
        #region FIREBASE COMMAND QUEUE

        private const string SERVER_VALUE_TIMESTAMP = "{\".sv\": \"timestamp\"}";

        protected enum FirebaseCommand
        {
            Get = 0,
            Set = 1,
            Update = 2,
            Push = 3,
            Delete = 4
        }

        protected class CommandLinkedList
        {
            public Firebase firebase;
            private FirebaseCommand _command;
            private string _param;
            private object _valObj;
            private string _valStr;
            private bool _isJson;
            public CommandLinkedList linkListNext;

            public CommandLinkedList(Firebase firebase, FirebaseCommand command, string param)
            {
                this.firebase = firebase;
                _command = command;
                _param = param;
                _valObj = null;
                _valStr = null;
                linkListNext = null;
            }

            public CommandLinkedList(Firebase firebase, FirebaseCommand command, string param, object valObj)
            {
                this.firebase = firebase;
                _command = command;
                _param = param;
                _valObj = valObj;
                _valStr = null;
                linkListNext = null;
            }

            public CommandLinkedList(Firebase firebase, FirebaseCommand command, string param, string valStr, bool isJson)
            {
                this.firebase = firebase;
                _command = command;
                _param = param;
                _valObj = null;
                _valStr = valStr;
                _isJson = isJson;
                linkListNext = null;
            }

            public void AddNext(CommandLinkedList next)
            {
                linkListNext = next;
            }

            public void DoCommand()
            {
                switch (_command)
                {
                    case FirebaseCommand.Get:
                        firebase.GetValue(_param);
                        break;
                    case FirebaseCommand.Set:
                        if (_valObj != null)
                            firebase.SetValue(_valObj, _param);
                        else
                            firebase.SetValue(_valStr, _isJson, _param);
                        break;
                    case FirebaseCommand.Update:
                        if (_valObj != null)
                            firebase.UpdateValueObject(_valObj, _param);
                        else
                            firebase.UpdateValue(_valStr, _param);
                        break;
                    case FirebaseCommand.Push:
                        if (_valObj != null)
                            firebase.Push(_valObj, _param);
                        else
                            firebase.Push(_valStr, _isJson, _param);
                        break;
                    case FirebaseCommand.Delete:
                        firebase.Delete(_param);
                        break;
                }
            }
        }

        private Action _onQueueCompleted;
        private Action _onQueueInterrupted;

        protected CommandLinkedList head;
        protected CommandLinkedList tail;
        protected bool autoStart;
        protected float retryWait;
        protected int retryCounterLimit;
        protected int count;
        protected int retryCounter;
        private IDisposable _commandDispose;

        protected void AddQueue(Firebase firebase, FirebaseCommand command, string param)
        {
            var commandNode = new CommandLinkedList(firebase, command, param);
            InsertNodeToQueue(commandNode);
        }

        protected void AddQueue(Firebase firebase, FirebaseCommand command, string param, string valStr, bool parseToJson)
        {
            var commandNode = new CommandLinkedList(firebase, command, param, valStr, parseToJson);
            InsertNodeToQueue(commandNode);
        }

        protected void AddQueue(Firebase firebase, FirebaseCommand command, string param, object valObj)
        {
            var commandNode = new CommandLinkedList(firebase, command, param, valObj);
            InsertNodeToQueue(commandNode);
        }

        private void InsertNodeToQueue(CommandLinkedList commandNode)
        {
            if (head == null)
            {
                head = commandNode;
                tail = commandNode;

                if (autoStart)
                    head.DoCommand();
            }
            else
            {
                tail.linkListNext = commandNode;
                tail = commandNode;
            }

            ++count;
        }

        protected void ClearQueueTopDown(CommandLinkedList node)
        {
            if (node == null)
                return;

            var temp = node.linkListNext;
            ClearQueueTopDown(temp);
            node.linkListNext = null;
        }

        protected void StartNextCommand()
        {
            head = head.linkListNext;
            Start();
        }


        protected void OnSuccess(Firebase sender, DataSnapshot snapshot)
        {
            --count;
            StartNextCommand();
            ClearCallbacks(sender);
        }

        protected void OnFailed(Firebase sender, FirebaseError err)
        {
            if (retryCounter < retryCounterLimit)
            {
                _commandDispose = Observable.Timer(TimeSpan.FromSeconds(retryWait)).Subscribe(_ =>
                {
                    retryCounter++;
                    head.DoCommand(); // Redo last command.
                });
            }
            else
            {
                _onQueueInterrupted?.Invoke();
            }
        }

        protected void ClearCallbacks(Firebase sender)
        {
            sender.onGetSuccess -= OnSuccess;
            sender.onGetFailed -= OnFailed;
            sender.onUpdateSuccess -= OnSuccess;
            sender.onUpdateFailed -= OnFailed;
            sender.onPushSuccess -= OnSuccess;
            sender.onPushFailed -= OnFailed;
            sender.onDeleteSuccess -= OnSuccess;
            sender.onDeleteFailed -= OnFailed;
        }

        #endregion

        #region PUBLIC FUNCTIONS

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SimpleFirebaseUnity.FirebaseQueue"/> class.
        /// </summary>
        /// <param name="autoStart">If set to <c>true</c> auto start when a queue added.</param>
        /// <param name="retryCounterLimit">Number of retries allowed when a request got an error. After limit reached, next command in queue will be stopped (can be restarted manually, starting from the last uncompleted command in queue).</param>
        /// <param name="retryWait">Wait duration of each retries in seconds.</param>
        /// <param name="onQueueCompleted">Callback which is called when process on last command in queue completed.</param>
        /// <param name="onQueueInterrupted">Callback which is called when queue process stopped before completing last command.</param>
        public FirebaseQueue(bool autoStart, int retryCounterLimit, float retryWait, Action onQueueCompleted, Action onQueueInterrupted)
        {
            Init(autoStart, retryCounterLimit, retryWait, onQueueCompleted, onQueueInterrupted);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SimpleFirebaseUnity.FirebaseQueue"/> class.
        /// </summary>
        /// <param name="autoStart">If set to <c>true</c> auto start when a queue added.</param>
        /// <param name="retryCounterLimit">Number of retries allowed when a request got an error. After limit reached, next command in queue will be stopped (can be restarted manually, starting from the last uncompleted command in queue).</param>
        /// <param name="retryWait">Wait duration of each retries in seconds.</param>
        public FirebaseQueue(bool autoStart, int retryCounterLimit, float retryWait)
        {
            Init(autoStart, retryCounterLimit, retryWait, null, null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SimpleFirebaseUnity.FirebaseQueue"/> class.
        /// </summary>
        /// <param name="autoStart">If set to <c>true</c> auto start when a queue added.</param>
        /// <param name="onQueueCompleted">Callback which is called when process on last command in queue completed.</param>
        /// <param name="onQueueInterrupted">Callback which is called when queue process stopped before completing last command.</param>
        public FirebaseQueue(bool autoStart, Action onQueueCompleted, Action onQueueInterrupted)
        {
            Init(autoStart, 0, float.MaxValue, onQueueCompleted, onQueueInterrupted);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SimpleFirebaseUnity.FirebaseQueue"/> class.
        /// </summary>
        /// <param name="autoStart">If set to <c>true</c> auto start when a queue added.</param>
        public FirebaseQueue(bool autoStart)
        {
            Init(autoStart, 0, float.MaxValue, null, null);
        }

        private void Init(bool autoStartParam, int retryCounterLimitParam, float retryWaitParam, Action onQueueCompleted, Action onQueueInterrupted)
        {
            autoStart = autoStartParam;
            retryCounterLimit = retryCounterLimitParam;
            retryWait = retryWaitParam;
            retryCounter = 0;
            count = 0;

            _onQueueCompleted = onQueueCompleted;
            _onQueueInterrupted = onQueueInterrupted;
        }

        /// <summary>
        /// Start processing the queue until all commands completed, or, an error occured and number of allowed retries is over the limit.
        /// </summary>
        public void Start()
        {
            retryCounter = 0;

            if (head != null)
                head.DoCommand();
            else
            {
                tail = null;
                _onQueueCompleted?.Invoke();
            }
        }

        /// <summary>
        /// Gets the number of request in queue.
        /// </summary>
        /// <value>The count.</value>
        public int Count => count;

        /// <summary>
        /// Determines whether the queue is empty.
        /// </summary>
        /// <returns><c>true</c> if this instance is empty; otherwise, <c>false</c>.</returns>
        public bool IsEmpty()
        {
            return (count == 0);
        }

        /// <summary>
        /// Adds Firebase Get request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueGet(Firebase firebase, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onGetSuccess += OnSuccess;
            temp.onGetFailed += OnFailed;
            AddQueue(temp, FirebaseCommand.Get, param);
        }

        /// <summary>
        /// Adds Firebase Get request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueGet(Firebase firebase, FirebaseParam param)
        {
            var temp = firebase.Copy(true);
            temp.onGetSuccess += OnSuccess;
            temp.onGetFailed += OnFailed;
            AddQueue(temp, FirebaseCommand.Get, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Set request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueSet(Firebase firebase, object val, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onSetSuccess += OnSuccess;
            temp.onSetFailed += OnFailed;
            AddQueue(temp, FirebaseCommand.Set, param, val);
        }

        /// <summary>
        /// Adds Firebase Set request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueSet(Firebase firebase, object val, FirebaseParam param)
        {
            AddQueueSet(firebase, val, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Set request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueSet(Firebase firebase, object val, float priority, string param = "")
        {
            var tempDict = new Dictionary<string, object> {{".value", val}, {".priority", priority}};

            AddQueueSet(firebase, tempDict, param);
        }

        /// <summary>
        /// Adds Firebase Set request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueSet(Firebase firebase, object val, float priority, FirebaseParam param)
        {
            var tempDict = new Dictionary<string, object> {{".value", val}, {".priority", priority}};

            AddQueueSet(firebase, tempDict, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Set request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Set value.</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueSet(Firebase firebase, string val, bool isJson, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onSetSuccess += OnSuccess;
            temp.onSetFailed += OnFailed;

            AddQueue(temp, FirebaseCommand.Set, param, val, isJson);
        }

        /// <summary>
        /// Adds Firebase Set request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueSet(Firebase firebase, string val, bool isJson, FirebaseParam param)
        {
            AddQueueSet(firebase, val, isJson, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Update request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueUpdate(Firebase firebase, Dictionary<string, object> val, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onSetSuccess += OnSuccess;
            temp.onSetFailed += OnFailed;

            AddQueue(firebase, FirebaseCommand.Update, param, val);
        }

        /// <summary>
        /// Adds Firebase Update request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueUpdate(Firebase firebase, Dictionary<string, object> val, FirebaseParam param)
        {
            AddQueueUpdate(firebase, val, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Update request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="valJson">Value in json format.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueUpdate(Firebase firebase, string valJson, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onUpdateSuccess += OnSuccess;
            temp.onUpdateFailed += OnFailed;

            AddQueue(temp, FirebaseCommand.Update, param, valJson, false); // Update value is strictly json.
        }

        /// <summary>
        /// Adds Firebase Update request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="valJson">Value in json format.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueUpdate(Firebase firebase, string valJson, FirebaseParam param)
        {
            AddQueueUpdate(firebase, valJson, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Push request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueuePush(Firebase firebase, object val, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onPushSuccess += OnSuccess;
            temp.onPushFailed += OnFailed;
            AddQueue(temp, FirebaseCommand.Push, param, Json.Serialize(val));
        }

        /// <summary>
        /// Adds Firebase Push request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueuePush(Firebase firebase, object val, FirebaseParam param)
        {
            AddQueuePush(firebase, val, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Push request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueuePush(Firebase firebase, object val, float priority, string param = "")
        {
            var tempDict = new Dictionary<string, object> {{".value", val}, {".priority", priority}};
            AddQueuePush(firebase, tempDict, param);
        }

        /// <summary>
        /// Adds Firebase Push request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueuePush(Firebase firebase, object val, float priority, FirebaseParam param)
        {
            var tempDict = new Dictionary<string, object> {{".value", val}, {".priority", priority}};
            AddQueuePush(firebase, tempDict, param);
        }

        /// <summary>
        /// Adds Firebase Push request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueuePush(Firebase firebase, string val, bool isJson, string param = "")
        {
            var temp = firebase.Copy(true);
            temp.onPushSuccess += OnSuccess;
            temp.onPushFailed += OnFailed;

            Debug.Log(val);
            AddQueue(temp, FirebaseCommand.Push, param, val, isJson);
        }

        /// <summary>
        /// Adds Firebase Push request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="val">Value.</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueuePush(Firebase firebase, string val, bool isJson, FirebaseParam param)
        {
            AddQueuePush(firebase, val, isJson, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Delete request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueDelete(Firebase firebase, string param = "")
        {
            Firebase temp = firebase.Copy(true);
            temp.onDeleteSuccess += OnSuccess;
            temp.onDeleteFailed += OnFailed;
            AddQueue(temp, FirebaseCommand.Delete, param);
        }

        /// <summary>
        /// Adds Firebase Delete request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="param">Parameter.</param>
        public void AddQueueDelete(Firebase firebase, FirebaseParam param)
        {
            AddQueueDelete(firebase, param.Parameter);
        }

        /// <summary>
        /// Adds Firebase Set Time Stamp request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="keyName">Time stamp key name.</param>
        public void AddQueueSetTimeStamp(Firebase firebase, string keyName)
        {
            var temp = firebase.Child(keyName);
            AddQueueSet(temp, SERVER_VALUE_TIMESTAMP, true, "print=silent");
        }

        /// <summary>
        /// Adds Firebase Set Time Stamp request with callback to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="keyName">Key name.</param>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On fail callback.</param>
        public void AddQueueSetTimeStamp(Firebase firebase, string keyName, Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed)
        {
            var temp = firebase.Child(keyName);
            temp.onSetSuccess += onSuccess;
            temp.onSetFailed += onFailed;

            AddQueueSet(temp, SERVER_VALUE_TIMESTAMP, true);
        }

        /// <summary>
        /// Adds Firebase Set Priority request to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="priority">Time stamp key name.</param>
        public void AddQueueSetPriority(Firebase firebase, float priority)
        {
            var temp = firebase.Child(".priority");
            AddQueueSet(temp, priority, "print=silent");
        }

        /// <summary>
        /// Adds Firebase Set Priority request with callback to queue.
        /// </summary>
        /// <param name="firebase">Firebase node.</param>
        /// <param name="priority">Time stamp key name.</param>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On fail callback.</param>
        public void AddQueueSetPriority(Firebase firebase, float priority, Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed)
        {
            var temp = firebase.Child(".priority");
            temp.onSetSuccess += onSuccess;
            temp.onSetFailed += onFailed;

            AddQueueSet(temp, priority);
        }


        /// <summary>
        /// Force stop the command queue chain and clears the queue. Warning: processed command cannot be undone.
        /// </summary>
        public void ForceClearQueue()
        {
            _commandDispose?.Dispose();
            ClearQueueTopDown(head);
            head = null;
            tail = null;
            GC.Collect();
        }

        #endregion
    }
}