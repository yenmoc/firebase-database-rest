﻿/*

Class: FirebaseObserver.cs
==============================================
Last update: 2018-05-20  (by Dikra)
==============================================


 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;
using System.Collections;
using UniRx;
using UnityEngine;

// ReSharper disable MemberCanBePrivate.Global

// ReSharper disable once CheckNamespace
namespace UnityModule.RestFirebase
{
    // ReSharper disable once UnusedMember.Global
    public class FirebaseObserver
    {
        public Action<Firebase, DataSnapshot> onChange;

        protected Firebase firebase;
        protected Firebase target;
        protected float refreshRate;
        protected string getParam;
        protected bool active;

        protected bool firstTime;
        protected DataSnapshot lastSnapshot;
        public IDisposable disposableRoutine;


        #region CONSTRUCTORS

        /// <summary>
        /// Creates an Observer that calls GetValue request at the given refresh rate (in seconds) and checks whether the value has changed.
        /// </summary>
        /// <param name="firebase">Firebase.</param>
        /// <param name="refreshRate">Refresh rate (in seconds).</param>
        /// <param name="getParam">Parameter value for the Get request that will be called periodically.</param>
        public FirebaseObserver(Firebase firebase, float refreshRate, string getParam = "")
        {
            active = false;
            lastSnapshot = null;
            this.firebase = firebase;
            this.refreshRate = refreshRate;
            this.getParam = getParam;
            target = firebase.Copy();
            disposableRoutine = null;
        }

        /// <summary>
        /// Creates an Observer that calls GetValue request at the given refresh rate (in seconds) and checks whether the value has changed.
        /// </summary>
        /// <param name="firebase">Firebase.</param>
        /// <param name="refreshRate">Refresh rate (in seconds).</param>
        /// <param name="getParam">Parameter value for the Get request that will be called periodically.</param>
        public FirebaseObserver(Firebase firebase, float refreshRate, FirebaseParam getParam)
        {
            active = false;
            lastSnapshot = null;
            this.firebase = firebase;
            this.refreshRate = refreshRate;
            this.getParam = getParam.Parameter;
            target = firebase.Copy();
        }

        #endregion

        #region OBSERVER FUNCTIONS

        /// <summary>
        /// Start the observer.
        /// </summary>
        public void Start()
        {
            if (disposableRoutine != null)
                Stop();

            active = true;
            firstTime = true;

            target.onGetSuccess += CompareSnapshot;

            disposableRoutine = Observable.FromCoroutine(RefreshCoroutine).Subscribe();
        }

        /// <summary>
        /// Stop the observer.
        /// </summary>
        private void Stop()
        {
            active = false;
            // ReSharper disable once DelegateSubtraction
            target.onGetSuccess -= CompareSnapshot;
            lastSnapshot = null;

            disposableRoutine?.Dispose();
        }

        private IEnumerator RefreshCoroutine()
        {
            while (active)
            {
                target.GetValue();
                yield return new WaitForSeconds(refreshRate);
            }
        }

        private void CompareSnapshot(Firebase dummyVar, DataSnapshot snapshot)
        {
            if (firstTime)
            {
                firstTime = false;
                lastSnapshot = snapshot;
                return;
            }

            if (snapshot != null)
            {
                if (lastSnapshot == null || (lastSnapshot != null && !lastSnapshot.RawJson.Equals(snapshot.RawJson)))
                {
                    onChange?.Invoke(firebase, snapshot);
                }
            }

            lastSnapshot = snapshot;
        }

        #endregion
    }
}