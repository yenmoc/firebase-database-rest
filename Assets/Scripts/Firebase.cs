﻿/*

Class: Firebase.cs
==============================================
Last update: 2018-05-20  (by Dikra)
==============================================

 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
using UnityModule.RestFirebase.MiniJSON;
// ReSharper disable FieldCanBeMadeReadOnly.Local

// ReSharper disable FieldCanBeMadeReadOnly.Global

// ReSharper disable UnusedMember.Global

#pragma warning disable 618

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.RestFirebase
{
#if !UNITY_WEBGL
    using System.Threading;

#endif

    [Serializable]
    public class Firebase
    {
        public const string SERVER_VALUE_TIMESTAMP = "{\".sv\": \"timestamp\"}";

        public Action<Firebase, DataSnapshot> onGetSuccess;
        public Action<Firebase, FirebaseError> onGetFailed;

        public Action<Firebase, DataSnapshot> onSetSuccess;
        public Action<Firebase, FirebaseError> onSetFailed;

        public Action<Firebase, DataSnapshot> onUpdateSuccess;
        public Action<Firebase, FirebaseError> onUpdateFailed;

        public Action<Firebase, DataSnapshot> onPushSuccess;
        public Action<Firebase, FirebaseError> onPushFailed;

        public Action<Firebase, DataSnapshot> onDeleteSuccess;
        public Action<Firebase, FirebaseError> onDeleteFailed;

        protected Firebase parent;
        internal FirebaseRoot root;
        protected string key;
        protected string fullKey;

        #region JSON PARSE THREADING

        private class ParserObjToJson
        {
            public bool isDone;
            public string json;

            private object _sourceObj;

            public ParserObjToJson(object sourceObj)
            {
                isDone = false;
                _sourceObj = sourceObj;

#if UNITY_WEBGL
                Run();
#else
                var parseThread = new Thread(Run);
                parseThread.Start();
#endif
            }

            private void Run()
            {
                json = Json.Serialize(_sourceObj);
                isDone = true;
            }
        }

        private static IEnumerator JsonSerializeRoutine(object sourceObj, FirebaseParam query, Action<string, FirebaseParam> onCompleted)
        {
            var parser = new ParserObjToJson(sourceObj);

            while (!parser.isDone)
                yield return null;

            onCompleted?.Invoke(parser.json, query);
        }

        private class ParserJsonToSnapshot
        {
            public bool isDone;
            public DataSnapshot snapshot;
            private string _json;

            public ParserJsonToSnapshot(string json)
            {
                isDone = false;
                _json = json;
#if UNITY_WEBGL
                Run();
#else
                var parseThread = new Thread(Run);
                parseThread.Start();
#endif
            }

            private void Run()
            {
                snapshot = new DataSnapshot(_json);
                isDone = true;
            }
        }

        #endregion

        #region GET-SET

        /// <summary>
        /// Parent of current firebase pointer
        /// </summary>                 
        public Firebase Parent => parent;

        /// <summary>
        /// Root firebase pointer of the endpoint
        /// </summary>
        public Firebase Root => root;

        /// <summary>
        /// Returns .json endpoint to this Firebase point
        /// </summary>
        public virtual string Endpoint => "https://" + Host + FullKey + "/.json";

        /// <summary>
        /// Returns main host of Firebase
        /// </summary>
        // ReSharper disable once MemberCanBeProtected.Global
        public virtual string Host => root.Host;

        /// <summary>
        /// Returns full key path to current pointer from root endpoint
        /// </summary>
        public string FullKey => fullKey;

        /// <summary>
        /// Returns key of current pointer
        /// </summary>
        public string Key => key;

        /// <summary>
        /// Credential for auth parameter. If no credential set to empty string
        /// </summary>
        protected virtual string Credential
        {
            get => root.Credential;
            set => root.Credential = value;
        }

        /// <summary>
        /// Gets the rules endpoint.
        /// </summary>
        /// <value>The rules endpoint.</value>
        // ReSharper disable once MemberCanBeProtected.Global
        public virtual string RulesEndpoint => root.RulesEndpoint;


        /**** CONSTRUCTOR ****/

        /// <summary>
        /// Create new Firebase endpoint
        /// </summary>
        /// <param name="parent">Parent Firebase pointer</param>
        /// <param name="key">Key under parent Firebase</param>
        /// <param name="root">Root Firebase pointer</param>
        /// <param name="inheritCallback">If set to <c>true</c> inherit callback.</param>
        internal Firebase(Firebase parent, string key, FirebaseRoot root, bool inheritCallback = false)
        {
            this.parent = parent;
            this.key = key;
            this.root = root;

            fullKey = this.parent.FullKey + "/" + this.key;

            if (inheritCallback)
            {
                onGetSuccess = this.parent.onGetSuccess;
                onGetFailed = this.parent.onGetFailed;

                onSetSuccess = this.parent.onSetSuccess;
                onSetFailed = this.parent.onSetFailed;

                onUpdateSuccess = this.parent.onUpdateSuccess;
                onUpdateFailed = this.parent.onUpdateFailed;

                onPushSuccess = this.parent.onPushSuccess;
                onPushFailed = this.parent.onPushFailed;

                onDeleteSuccess = this.parent.onDeleteSuccess;
                onDeleteFailed = this.parent.onDeleteFailed;
            }
        }

        internal Firebase()
        {
            parent = null;
            key = string.Empty;
            root = null;
        }

        #endregion

        #region BASIC FUNCTIONS

        /// <summary>
        /// Get Firebase child from given key
        /// </summary>
        /// <param name="key">A string</param>
        /// <param name="inheritCallback">If set to <c>true</c> inherit callback.</param>
        public Firebase Child(string key, bool inheritCallback = false)
        {
            return new Firebase(this, key, root, inheritCallback);
        }

        /// <summary>
        /// Get Firebase childs from given keys
        /// </summary>
        /// <param name="keys">List of string</param>
        public Firebase[] Childs(List<string> keys)
        {
            var childs = new Firebase[keys.Count];
            for (var i = 0; i < keys.Count; i++)
            {
                childs[i] = (Child(keys[i]));
            }

            return childs;
        }

        public Firebase ChildParams(bool inheritCallback, params string[] keys)
        {
            var childs = new Firebase[keys.Length];
            for (var i = 0; i < keys.Length; i++)
            {
                childs[i] = (Child(keys[i], true));
            }

            if (childs.Length == 0)
            {
                return null;
            }

            var firebase = childs[0];
            for (var i = 1; i < childs.Length; i++)
            {
                firebase = firebase.Child($"{childs[i].key}", inheritCallback);
            }

            return firebase;
        }

        /// <summary>
        /// Get Firebase childs from given keys
        /// </summary>
        /// <param name="keys">Array of string</param>
        public Firebase[] Childs(string[] keys)
        {
            var childs = new Firebase[keys.Length];
            for (var i = 0; i < keys.Length; i++)
            {
                childs[i] = (Child(keys[i], true));
            }

            return childs;
        }

        /// <summary>
        /// Get a fresh copy of this Firebase object
        /// </summary>
        /// <param name="inheritCallback">If set to <c>true</c> inherit callback.</param>
        public Firebase Copy(bool inheritCallback = false)
        {
            var temp = parent == null ? root.Copy() : new Firebase(parent, key, root);

            if (!inheritCallback) return temp;
            temp.onGetSuccess = onGetSuccess;
            temp.onGetFailed = onGetFailed;

            temp.onSetSuccess = onSetSuccess;
            temp.onSetFailed = onSetFailed;

            temp.onUpdateSuccess = onUpdateSuccess;
            temp.onUpdateFailed = onUpdateFailed;

            temp.onPushSuccess = onPushSuccess;
            temp.onPushFailed = onPushFailed;

            temp.onDeleteSuccess = onDeleteSuccess;
            temp.onDeleteFailed = onDeleteFailed;

            return temp;
        }

        #endregion

        #region REST FUNCTIONS

        /*** GET ***/

        /// <summary>
        /// Fetch data from Firebase. Calls OnGetSuccess on success, OnGetFailed on failed.
        /// OnGetSuccess action contains the corresponding Firebase and the fetched Snapshot
        /// OnGetFailed action contains the error exception
        /// </summary>
        /// <param name="param">REST call parameters on a string. Example: &quot;orderBy=&#92;"$key&#92;"&quot;print=pretty&quot;shallow=true"></param>
        /// <returns></returns>
        public void GetValue(string param = "")
        {
            GetValue(new FirebaseParam(param));
        }

        /// <summary>
        /// Fetch data from Firebase. Calls OnGetSuccess on success, OnGetFailed on failed.
        /// OnGetSuccess action contains the corresponding Firebase and the fetched Snapshot
        /// OnGetFailed action contains the error exception
        /// </summary>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void GetValue(FirebaseParam query)
        {
            try
            {
                if (Credential != "")
                {
                    query = new FirebaseParam(query).Auth(Credential);
                }

                var url = Endpoint;
                var param = UnityWebRequest.EscapeURL(query.Parameter);

                if (param != "")
                    url += "?" + param;

                Observable.FromCoroutine(() => RequestCoroutine(url, null, query.HttpHeader, onGetSuccess, onGetFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onGetFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onGetFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }

        /*** SET ***/

        /// <summary>
        /// Set value of a key on Firebase. Calls OnSetSuccess on success, OnSetFailed on failed.
        /// OnSetSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnSetFailed action contains the error exception
        /// </summary>
        /// <param name="valJson">Set value in json format</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        private void SetValueJson(string valJson, FirebaseParam query)
        {
            try
            {
                if (Credential != "")
                {
                    query = new FirebaseParam(query).Auth(Credential);
                }

                var url = Endpoint;

                var param = UnityWebRequest.EscapeURL(query.Parameter);

                if (param != "")
                    url += "?" + param;


                var headers = new Dictionary<string, string>();

                foreach (var kv in query.HttpHeader)
                    headers.Add(kv.Key, kv.Value);

                headers.Add("Content-Type", "application/json");

                if (!headers.ContainsKey("X-HTTP-Method-Override"))
                    headers.Add("X-HTTP-Method-Override", "PUT");

                //UTF8Encoding encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(valJson);

                Observable.FromCoroutine(() => RequestCoroutine(url, bytes, headers, onSetSuccess, onSetFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onSetFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onSetFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }

        /// <summary>
        /// Set value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="val">Set value</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void SetValue(string val, bool isJson, string param = "")
        {
            SetValue(val, isJson, new FirebaseParam(param));
        }

        /// <summary>
        /// Set value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnSetSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnSetFailed action contains the error exception
        /// </summary>
        /// <param name="val">Set value</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void SetValue(string val, bool isJson, FirebaseParam query)
        {
            if (isJson)
                SetValueJson(val, query);
            else
                Observable.FromCoroutine(() => JsonSerializeRoutine(val, query, SetValueJson)).Subscribe();
        }

        /// <summary>
        /// Set value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnSetSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnSetFailed action contains the error exception
        /// </summary>
        /// <param name="val">Set value</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void SetValue(object val, string param = "")
        {
            SetValue(val, new FirebaseParam(param));
        }

        /// <summary>
        /// Set value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnSetSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnSetFailed action contains the error exception
        /// </summary>
        /// <param name="val">Set value</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void SetValue(object val, FirebaseParam query)
        {
            Observable.FromCoroutine(() => JsonSerializeRoutine(val, query, SetValueJson)).Subscribe();
        }

        /// <summary>
        /// Set value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnSetSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnSetFailed action contains the error exception
        /// </summary>
        /// <param name="val">Set value</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void SetValue(object val, float priority, string param = "")
        {
            SetValue(val, priority, new FirebaseParam(param));
        }

        /// <summary>
        /// Set value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnSetSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnSetFailed action contains the error exception
        /// </summary>
        /// <param name="val">Set value</param>
        /// <param name="priority">Priority.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void SetValue(object val, float priority, FirebaseParam query)
        {
            var tempDict = new Dictionary<string, object> {{".value", val}, {".priority", priority}};
            Observable.FromCoroutine(() => JsonSerializeRoutine(tempDict, query, SetValueJson)).Subscribe();
        }


        /*** UPDATE ***/

        /// <summary>
        /// Update value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="valJson">Update value in json format</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void UpdateValue(string valJson, FirebaseParam query)
        {
            try
            {
                if (Credential != "")
                {
                    query = new FirebaseParam(query).Auth(Credential);
                }

                var url = Endpoint;

                var param = UnityWebRequest.EscapeURL(query.Parameter);

                if (param != string.Empty)
                    url += "?" + param;

                var headers = new Dictionary<string, string>();

                foreach (var kv in query.HttpHeader)
                    headers.Add(kv.Key, kv.Value);

                headers.Add("Content-Type", "application/json");

                if (!headers.ContainsKey("X-HTTP-Method-Override"))
                    headers.Add("X-HTTP-Method-Override", "PATCH");

                //UTF8Encoding encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(valJson);

                Observable.FromCoroutine(() => RequestCoroutine(url, bytes, headers, onUpdateSuccess, onUpdateFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onUpdateFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onUpdateFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }

        /// <summary>
        /// Update value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="val">Update value</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void UpdateValue(Dictionary<string, object> val, string param = "")
        {
            UpdateValue(val, new FirebaseParam(param));
        }

        /// <summary>
        /// Update value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="val">Update value</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void UpdateValue(Dictionary<string, object> val, FirebaseParam query)
        {
            Observable.FromCoroutine(() => JsonSerializeRoutine(val, query, UpdateValue)).Subscribe();
        }

        /// <summary>
        /// Update value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="valJson">Update value</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void UpdateValue(string valJson, string param = "")
        {
            UpdateValue(valJson, new FirebaseParam(param));
        }

        /// <summary>
        /// Update value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="val">Update value</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        internal void UpdateValueObject(object val, FirebaseParam query)
        {
            if (!(val is Dictionary<string, object>))
            {
                onUpdateFailed?.Invoke(this, new FirebaseError((HttpStatusCode) 400, "Invalid data; couldn't parse JSON object. Are you sending a JSON object with valid key names?"));

                return;
            }

            Observable.FromCoroutine(() => JsonSerializeRoutine(val, query, UpdateValue)).Subscribe();
        }

        /// <summary>
        /// Update value of a key on Firebase. Calls OnUpdateSuccess on success, OnUpdateFailed on failed.
        /// OnUpdateSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnUpdateFailed action contains the error exception
        /// </summary>
        /// <param name="val">Update value</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        internal void UpdateValueObject(object val, string param = "")
        {
            UpdateValueObject(val, new FirebaseParam(param));
        }

        /*** PUSH ***/

        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="valJson">Push value in json format</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        void PushJson(string valJson, FirebaseParam query)
        {
            try
            {
                if (Credential != "")
                {
                    query = new FirebaseParam(query).Auth(Credential);
                }

                var url = Endpoint;

                var param = UnityWebRequest.EscapeURL(query.Parameter);

                if (param != string.Empty)
                    url += "?" + param;

                //UTF8Encoding encoding = new UTF8Encoding();
                Debug.Log("before :" + valJson);
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(valJson);

                Observable.FromCoroutine(() => RequestCoroutine(url, bytes, query.HttpHeader, onPushSuccess, onPushFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onPushFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onPushFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }


        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="val">Push value</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void Push(string val, bool isJson, string param = "")
        {
            Push(val, isJson, new FirebaseParam(param));
        }

        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="val">Push value</param>
        /// <param name="isJson">True if string is an object parsed in a json string.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void Push(string val, bool isJson, FirebaseParam query)
        {
            if (isJson)
                PushJson(val, query);
            else
                Observable.FromCoroutine(() => JsonSerializeRoutine(val, query, PushJson)).Subscribe();
        }

        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="val">Push value</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void Push(object val, string param = "")
        {
            Push(val, new FirebaseParam(param));
        }

        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="val">Push value</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void Push(object val, FirebaseParam query)
        {
            Observable.FromCoroutine(() => JsonSerializeRoutine(val, query, PushJson)).Subscribe();
        }

        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="val">Push value</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void Push(object val, float priority, string param = "")
        {
            Push(val, priority, new FirebaseParam(param));
        }

        /// <summary>
        /// Push a value (with random new key) on a key in Firebase. Calls OnPushSuccess on success, OnPushFailed on failed.
        /// OnPushSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnPushFailed action contains the error exception
        /// </summary>
        /// <param name="val">Push value</param>
        /// <param name="priority">Priority.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void Push(object val, float priority, FirebaseParam query)
        {
            var tempDict = new Dictionary<string, object> {{".value", val}, {".priority", priority}};
            Observable.FromCoroutine(() => JsonSerializeRoutine(tempDict, query, PushJson)).Subscribe();
        }

        /*** DELETE ***/

        /// <summary>
        /// Delete a key in Firebase. Calls OnDeleteSuccess on success, OnDeleteFailed on failed.
        /// OnDeleteSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnDeleteFailed action contains the error exception
        /// </summary>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        /// <returns></returns>
        public void Delete(FirebaseParam query)
        {
            try
            {
                if (Credential != "")
                {
                    query = new FirebaseParam(query).Auth(Credential);
                }

                var url = Endpoint;

                var param = UnityWebRequest.EscapeURL(query.Parameter);

                if (param != string.Empty)
                    url += "?" + param;

                var headers = new Dictionary<string, string>();

                foreach (var kv in query.HttpHeader)
                    headers.Add(kv.Key, kv.Value);

                headers.Add("Content-Type", "application/json");

                if (!headers.ContainsKey("X-HTTP-Method-Override"))
                    headers.Add("X-HTTP-Method-Override", "DELETE");

                //UTF8Encoding encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes("{ \"dummy\" : \"dummies\"}");

                Observable.FromCoroutine(() => RequestCoroutine(url, bytes, headers, onDeleteSuccess, onDeleteFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onDeleteFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onDeleteFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }

        /// <summary>
        /// Delete a key in Firebase. Calls OnDeleteSuccess on success, OnDeleteFailed on failed.
        /// OnDeleteSuccess action contains the corresponding Firebase and the response Snapshot
        /// OnDeleteFailed action contains the error exception
        /// </summary>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        /// <returns></returns>
        public void Delete(string param = "")
        {
            Delete(new FirebaseParam(param));
        }

        /*** PRIORITY ***/

        /// <summary>
        /// Sets the priority of the node.
        /// </summary>
        /// <param name="priority">Priority.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        public void SetPriority(float priority, string param = "")
        {
            SetPriority(priority, new FirebaseParam(param));
        }

        /// <summary>
        /// Sets the priority of the node.
        /// </summary>
        /// <param name="priority">Priority.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        public void SetPriority(float priority, FirebaseParam query)
        {
            Copy().Child(".priority").SetValue(priority, query);
        }

        /// <summary>
        /// Sets the priority of the node.
        /// </summary>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On failed callback.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        public void SetPriority(Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, float priority, string param = "")
        {
            SetPriority(onSuccess, onFailed, priority, new FirebaseParam(param));
        }

        /// <summary>
        /// Sets the priority of the node.
        /// </summary>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On failed callback.</param>
        /// <param name="priority">Priority.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        public void SetPriority(Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, float priority, FirebaseParam query)
        {
            var temp = Copy();
            temp.onSetSuccess += onSuccess;
            temp.onSetFailed += onFailed;
            temp.Child(".priority").SetValue(priority, query);
        }


        /*** TIME STAMP ***/

        /// <summary>
        /// Sets the time stamp with the time since UNIX epoch by server value (in milliseconds).
        /// </summary>
        /// <param name="keyName">Key name.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        public void SetTimeStamp(string keyName, string param = "")
        {
            SetTimeStamp(keyName, new FirebaseParam(param));
        }

        /// <summary>
        /// Sets the time stamp with the time since UNIX epoch by server value (in milliseconds).
        /// </summary>
        /// <param name="keyName">Key name.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        public void SetTimeStamp(string keyName, FirebaseParam query)
        {
            Copy().Child(keyName).SetValue(SERVER_VALUE_TIMESTAMP, true, query);
        }

        /// <summary>
        /// Sets the time stamp with the time since UNIX epoch by server value (in milliseconds).
        /// </summary>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On failed callback.</param>
        /// <param name="keyName">Key name.</param>
        /// <param name="param">REST call parameters on a string. Example: "auth=ASDF123"</param>
        public void SetTimeStamp(Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, string keyName, string param = "")
        {
            SetTimeStamp(onSuccess, onFailed, keyName, new FirebaseParam(param));
        }

        /// <summary>
        /// Sets the time stamp with the time since UNIX epoch by server value (in milliseconds).
        /// </summary>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On failed callback.</param>
        /// <param name="keyName">Key name.</param>
        /// <param name="query">REST call parameters wrapped in FirebaseQuery class</param>
        public void SetTimeStamp(Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, string keyName, FirebaseParam query)
        {
            var temp = Copy();
            temp.onSetSuccess += onSuccess;
            temp.onSetFailed += onFailed;
            temp.Child(keyName).SetValue(SERVER_VALUE_TIMESTAMP, true, query);
        }


        /*** RULES ***/

        /// <summary>
        /// Gets Firebase Rules. Returned value is treated the same as returned value on Get request, packaged in DataSnapshot. Please note that FIREBASE_SECRET is required. If secret parameter is not set, it will use the Credential that has been set when CreateNew called.
        /// </summary>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On failed callback.</param>
        /// <param name="secret">Firebase Secret.</param>
        public void GetRules(Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, string secret = "")
        {
            try
            {
                if (string.IsNullOrEmpty(secret))
                {
                    if (!string.IsNullOrEmpty(Credential))
                        secret = Credential;
                }

                var url = RulesEndpoint;

                url += "?auth=" + secret;

                Observable.FromCoroutine(() => RequestCoroutine(url, null, null, onSuccess, onFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }

        /// <summary>
        /// Sets Firebase Rules. Returned value is treated the same as returned value on Set request, packaged in DataSnapshot.Please note that FIREBASE_SECRET is required. If secret parameter is not set, it will use the Credential that has been set when CreateNew called.
        /// </summary>
        /// <param name="json">Valid rules Json.</param>
        /// <param name="onSuccess">On success callback.</param>
        /// <param name="onFailed">On failed callback.</param>
        /// <param name="secret">Firebase Secret.</param>
        public void SetRules(string json, Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, string secret = "")
        {
            try
            {
                if (string.IsNullOrEmpty(secret))
                {
                    if (!string.IsNullOrEmpty(Credential))
                        secret = Credential;
                }

                var url = RulesEndpoint;

                url += "?auth=" + secret;

                var headers = new Dictionary<string, string> {{"Content-Type", "application/json"}, {"X-HTTP-Method-Override", "PUT"}};

                //UTF8Encoding encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(json);

                Observable.FromCoroutine(() => RequestCoroutine(url, bytes, headers, onSuccess, onFailed)).Subscribe();
            }
            catch (WebException webEx)
            {
                onFailed?.Invoke(this, FirebaseError.Create(webEx));
            }
            catch (Exception ex)
            {
                onFailed?.Invoke(this, new FirebaseError(ex.Message));
            }
        }

        /// <summary>
        /// Sets Firebase Rules silently. Please note that FIREBASE_SECRET is required. If secret parameter is not set, it will use the Credential that has been set when CreateNew called.
        /// </summary>
        /// <param name="json">Valid rules Json.</param>
        /// <param name="secret">Firebase Secret.</param>
        public void SetRules(string json, string secret = "")
        {
            SetRules(json, null, null, secret);
        }

        /// <summary>
        /// Sets Firebase Rules silently. Please note that FIREBASE_SECRET is required. If secret parameter is not set, it will use the Credential that has been set when CreateNew called.Sets the rules.
        /// </summary>
        /// <param name="rules">Valid rules that could be serialized into json.</param>
        /// <param name="onSuccess">On success.</param>
        /// <param name="onFailed">On failed.</param>
        /// <param name="secret">Firebase Secret.</param>
        public void SetRules(Dictionary<string, object> rules, Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed, string secret = "")
        {
            SetRules(Json.Serialize(rules), onSuccess, onFailed, secret);
        }

        /// <summary>
        /// Sets Firebase Rules silently. Please note that FIREBASE_SECRET is required. If secret parameter is not set, it will use the Credential that has been set when CreateNew called.Sets the rules.
        /// </summary>
        /// <param name="rules">Valid rules that could be serialized into json.</param>
        /// <param name="secret">Firebase Secret.</param>
        public void SetRules(Dictionary<string, object> rules, string secret = "")
        {
            SetRules(Json.Serialize(rules), null, null, secret);
        }

        #endregion

        #region REQUEST COROUTINE

        protected IEnumerator RequestCoroutine(string url, byte[] postData, Dictionary<string, string> headers, Action<Firebase, DataSnapshot> onSuccess, Action<Firebase, FirebaseError> onFailed)
        {
            using (var www = (headers != null) ? new WWW(url, postData, headers) : (postData != null) ? new WWW(url, postData) : new WWW(url))
            {
                // Wait until load done
                yield return www;

                if (!string.IsNullOrEmpty(www.error))
                {
                    HttpStatusCode status = 0;
                    var errMessage = "";

                    // Parse status code
                    if (www.responseHeaders.ContainsKey("STATUS"))
                    {
                        var str = www.responseHeaders["STATUS"];
                        var components = str.Split(' ');
                        if (components.Length >= 3 && int.TryParse(components[1], out var code))
                            status = (HttpStatusCode) code;
                    }

                    if (www.error.Contains("crossdomain.xml") || www.error.Contains("Couldn't resolve"))
                    {
                        errMessage = "No internet connection or crossdomain.xml policy problem";
                    }
                    else
                    {
                        // Parse error message

                        try
                        {
                            if (!string.IsNullOrEmpty(www.text))
                            {
                                if (Json.Deserialize(www.text) is Dictionary<string, object> obj && obj.ContainsKey("error"))
                                    errMessage = obj["error"] as string;
                            }
                        }
                        catch
                        {
                            // ignored
                        }
                    }


                    if (onFailed != null)
                    {
                        if (string.IsNullOrEmpty(errMessage))
                            errMessage = www.error;

                        if (errMessage.Contains("Failed downloading"))
                        {
                            errMessage = "Request failed with no info of error.";
                        }

                        onFailed(this, new FirebaseError(status, errMessage));
                    }

#if UNITY_EDITOR
                    Debug.LogWarning(www.error + " (" + (int) status + ")\nResponse Message: " + errMessage);
#endif
                }
                else
                {
                    var parser = new ParserJsonToSnapshot(www.text);

                    while (!parser.isDone)
                        yield return null;

                    var snapshot = parser.snapshot;
                    onSuccess?.Invoke(this, snapshot);
                }
            }
        }

        #endregion

        #region STATIC FUNCTIONS

        /// <summary>
        /// Creates new Firebase pointer at a valid Firebase url
        /// </summary>
        /// <param name="host">Example: "hostname.firebaseio.com" </param>
        /// <param name="credential">Credential value for auth parameter</param>
        /// <returns></returns>
        public static Firebase CreateNew(string host, string credential = "")
        {
            if (host.StartsWith("https://"))
                host = host.Substring("https://".Length);

            return new FirebaseRoot(host, credential);
        }

        /// <summary>
        /// Converts unix time stamp into DateTime
        /// </summary>
        /// <returns>The stamp to date time.</returns>
        /// <param name="unixTimeStamp">Unix time stamp.</param>
        public static DateTime TimeStampToDateTime(long unixTimeStamp)
        {
            var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }

        #endregion
    }
}