﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace UnityModule.RestFirebase.Editor
{
    public class DeploymentEditor : EditorWindow
    {
        private Vector2 _scrollPosition;
        private string _databaseUrl = "";
        private string _databaseSecretKey = "";
        private string _json = "";
        private string _branch = "";
        public string version = "";
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private List<string> _keyParams = new List<string>();

        private const string KEY_SAVE_DATABASE_URL = "database_url_editor";
        private const string KEY_SAVE_SECRET_KEY = "database_secret_key_editor";

        [MenuItem("Window/Firebase/DeployData")]
        private static void ShowWindow()
        {
            var window = GetWindow(typeof(DeploymentEditor)) as DeploymentEditor;
            if (window == null) return;
            window.titleContent = new GUIContent("Deploy Data");
            window.ShowUtility();
        }

        private void OnGUI()
        {
            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUI.skin.scrollView);
            GUILayout.BeginVertical();
            {
                GUILayout.Label("Settings", EditorStyles.boldLabel);
                if (PlayerPrefs.HasKey(KEY_SAVE_DATABASE_URL) && string.IsNullOrEmpty(_databaseUrl))
                {
                    _databaseUrl = PlayerPrefs.GetString(KEY_SAVE_DATABASE_URL);
                }

                _databaseUrl = EditorGUILayout.TextField("Database url", _databaseUrl);

                if (PlayerPrefs.HasKey(KEY_SAVE_SECRET_KEY) && string.IsNullOrEmpty(_databaseSecretKey))
                {
                    _databaseSecretKey = PlayerPrefs.GetString(KEY_SAVE_SECRET_KEY);
                }

                _databaseSecretKey = EditorGUILayout.TextField("Database secret key", _databaseSecretKey);

                GUILayout.Label("");
                GUILayout.Label("Json data deploy", EditorStyles.boldLabel);
                _json = EditorGUILayout.TextField("Json Data", _json);

                GUILayout.Label("");
                GUILayout.Label("Path key deploy", EditorStyles.boldLabel);
                EditorGUILayout.HelpBox("These is tree key containt data deploy", MessageType.Info);

                var removeId = -1;


                GUILayout.BeginHorizontal();
                _branch = EditorGUILayout.TextField("Branch", _branch);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                version = EditorGUILayout.TextField("Version", version);
                GUILayout.EndHorizontal();


                for (var i = 0; i < _keyParams.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    _keyParams[i] = EditorGUILayout.TextField($"key {i}", _keyParams[i]);
                    if (GUILayout.Button("X", EditorStyles.toolbarButton, GUILayout.Width(20)))
                    {
                        removeId = i;
                    }

                    GUILayout.EndHorizontal();
                }

                if (removeId >= 0)
                    _keyParams.RemoveAt(removeId);

                if (GUILayout.Button("Add child key", GUILayout.Width(130)))
                {
                    _keyParams.Add("");
                }

                GUILayout.Label("");
                GUI.backgroundColor = Color.green;
                if (GUILayout.Button("Deploy"))
                {
                    Debug.Log("Start ....");
                    SaveSetting();
                    var keys = new List<string> {_branch, version};
                    keys.AddRange(_keyParams);
                    Observable.FromMicroCoroutine(() => IeDeploy(_json, true, keys.ToArray())).Subscribe();
                }
            }
            try
            {
                GUILayout.EndVertical();
                EditorGUILayout.EndScrollView();
            }
            catch (Exception)
            {
                //ignore
            }
        }

        private void SaveSetting()
        {
            PlayerPrefs.SetString(KEY_SAVE_DATABASE_URL, _databaseUrl);
            PlayerPrefs.SetString(KEY_SAVE_SECRET_KEY, _databaseSecretKey);
        }

        private IEnumerator IeDeploy(string json, bool inheritCallback, params string[] keys)
        {
            var firebase = Firebase.CreateNew(_databaseUrl, _databaseSecretKey);
            if (inheritCallback)
            {
                firebase.onSetSuccess += SetOkHandler;
            }

            yield return null;
            firebase.ChildParams(inheritCallback, keys).SetValue(json, true, FirebaseParam.Empty.PrintSilent());
        }

        private static void SetOkHandler(Firebase sender, DataSnapshot snapshot)
        {
            Debug.Log("[OK] Set from key: <" + sender.FullKey + ">");
            Debug.Log("data: " + snapshot.RawJson);
        }
    }
}